//Chris Hinson
//Chapter 4 Program 2
//Class to handle output

public class output {

    public static void printGraph(double speed, double hours)
    {
        System.out.println("Hour    Distance Traveled");
        System.out.println("-------------------------");
        for (int i=1; i<=hours; i++)
        {
        System.out.println(i + "       " + speed*i);
        }
    }
}
